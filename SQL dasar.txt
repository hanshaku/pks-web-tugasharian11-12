Soal 1 Membuat Database
CREATE DATABASE myshop; 

Soal 2 Membuat Table di Dalam Database
1.Tabel users
CREATE TABLE users( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255), email varchar(225), password varchar(225)); 
2.Tabel categories
CREATE TABLE categories( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255)); 
3.Tabel items
CREATE TABLE items( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255), description varchar(255), price int(9), stock int(5),category_id int(8), FOREIGN KEY(category_id) REFERENCES categories(id)); 

Soal 3 Memasukkan Data pada Table
1.Tabel users
INSERT INTO users(name, email, password) VALUES("John Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@doe.com", "jenita123"); 
2.Tabel categories
INSERT INTO categories(name) VALUES("gadget"), ("cloth"), ("men"), ("women"), ("branded"); 
3.Tabel items
INSERT INTO items(name, description, price, stock, category_id) VALUES("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1), ("Uniklooh", "baju keren dari brand ternama", 500000, 20 ,2), ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10 ,1); 